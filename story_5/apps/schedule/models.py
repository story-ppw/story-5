from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from datetime import date

# Create your models here.

TERM = [
    ('Genap', 'Genap'),
    ('Gasal', 'Gasal')
]

class Course(models.Model):
    name = models.CharField(max_length=50)
    lecturer = models.CharField(max_length=50)
    credit = models.IntegerField(
        validators=[
                MinValueValidator(1, message="The minimum credit is 1"), 
                MaxValueValidator(24, message="The maximum credit is 24")
            ]
        )
    description = models.TextField()
    year_parity = models.CharField(max_length=20, choices=TERM)
    start_year = models.IntegerField(
        validators=[
                MinValueValidator(2019, message="The minimum starting year is 2019"), 
                MaxValueValidator(date.today().year, message="The maximum starting year is " + str(date.today().year))
            ]
        )
    room = models.CharField(max_length=50)

    def __str__(self):
        return self.name
