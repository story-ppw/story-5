# Generated by Django 3.1.2 on 2020-10-15 03:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0002_auto_20201014_2335'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='lecturer',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='course',
            name='name',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name='course',
            name='room',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='course',
            name='year',
            field=models.CharField(max_length=20),
        ),
    ]
