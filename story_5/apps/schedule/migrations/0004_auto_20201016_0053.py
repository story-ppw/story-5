# Generated by Django 3.1.2 on 2020-10-15 17:53

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20201015_1059'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='year',
        ),
        migrations.AddField(
            model_name='course',
            name='start_year',
            field=models.IntegerField(default=2020, validators=[django.core.validators.MinValueValidator(2019)]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='course',
            name='year_parity',
            field=models.CharField(choices=[('GNP', 'Genap'), ('GSL', 'Gasal')], default='Gasal', max_length=20),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='course',
            name='credit',
            field=models.IntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(24)]),
        ),
        migrations.AlterField(
            model_name='course',
            name='name',
            field=models.CharField(max_length=50),
        ),
    ]
