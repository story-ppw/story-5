from django.shortcuts import render, get_object_or_404
from .forms import CourseForm
from .models import Course
from django.http import HttpResponseRedirect

# Create your views here.

def index(request):
    courses = Course.objects.all()
    if request.method == "GET":
        form = CourseForm(request.GET or None)
        if (form.is_valid()):
            form.save()
            return HttpResponseRedirect('/schedule')
    else:
        form = CourseForm()
    return render(request, 'schedule/index.html', {'form': form, 'courses': courses})

def delete(request, pk = None):
    course = get_object_or_404(Course, pk=pk)
    if request.method == "GET":
        course.delete()
        return HttpResponseRedirect('/schedule')
    return render(request, 'schedule/index.html')

def detail(request, pk=None):
    course = get_object_or_404(Course, pk=pk)
    if request.method == "GET":
        return render(request, 'schedule/course-detail.html', {'course': course})
