# Story 5

[![pipeline status][pipeline-badge]][commit]
[![coverage report][coverage-badge]][commit]

Repo ini akan digunakan untuk story 5

[pipeline-badge]: https://gitlab.com/story-ppw/story-5/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/story-ppw/story-5/badges/master/coverage.svg
[commit]: https://gitlab.com/story-ppw/story-5/-/commits/master
