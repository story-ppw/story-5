module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    colors: {
      green: {
        default: "#2A9D8F",
        light: "#20ADBA",
        dark: "#3A6179",
        darker: '#006471',
      },
      orange: {
        light: "#F4A261",
        dark: "#E76F51",
      },
      white: "#FFF",
      gray: "#EDF2F7",
      black: "#000",
    },
    fontFamily: {
      primary: ['"Montserrat Alternates"', "Arial", "sans-serif"],
      secondary: ['"Signika Negative"', "Arial", "sans-serif"],
      tertiary: ['Sarala', "Arial", "sans-serif"],
      header: ['Raleway', "Arial", "sans-serif"],
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
